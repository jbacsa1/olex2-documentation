---
title: "Getting Started"
linkTitle: "Getting Started"
weight: 2
type: docs
description: >
  Download, install and configure Olex2
categories:
 - Installation
tags:
 - download
 - versions
 - setup
 
---


It is easy to get started with Olex2. It runs on all common computer systems and no special system requirements need to be met.

Olex2 contains everything that is required to work with small-molecule crystal structures.
{{< svg "solve-refine-finish.svg" >}}


## Download and Installation

Olex2 runs on all common computer platforms. There is no complicated installation procedure and nothing needs to be compiled.

Download all version of Olex2 **[here](/olex2/docs/getting-started/installing-olex2/)**.

## Olex2 Versions

If you are new to the processing of crystal structures, we have the official release version of Olex2.	

This version is the most stable of Olex2 and is regularly updated. 
If you want earlier access to new features, you can try out the Beta or even Alpha version.

Be careful, sometimes vicious bugs make their way into these versions, but get usually dealt with rather sooner than later!

For more information about the different Olex2 versions see **[Olex2 Versions](/olex2/docs/getting-started/versions/)**.

## Setup

Be sure to read our **[Tips and Tricks](/olex2/docs/getting-started/getting-around-olex2/00_introduction/)** about how to set up your Olex2

## Find out more

Find out what Olex2 can do to improve your structures in our **[Documentation](/olex2/docs/getting-started/getting-around-olex2/).**
