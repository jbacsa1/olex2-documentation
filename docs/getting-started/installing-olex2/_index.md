---
title: "Installation Notes"
linkTitle: "Installation"
date: 2020-06-2020
weight: 10

categories:
 - Installation
 
tags:
 - Windows
 - Linux
 - MacOS
 
---

Installing Olex2 should be a simple procedure.

- Olex2 runs on all common computer platforms
- There is no complicated installation procedure
- Nothing needs to be compiled
- Everything you need to perform small-molecule structure solution and refinements is included

##  Windows

The Olex2 installer for windows is a small program which offers various installation options. You can select which **[version](../versions)** you would like to install.

{{< webp image="/images/win_installer.jpg" alt="The windows installer">}}

You can start with the **default settings** and explore different version when you need them. For example, you may want to run the _alpha_ version of Olex2 as well as the _Release_ version. In any case, each major version of Olex2 will automatically update within its own series.

<b>Your browser may block the direct download, so in that case please right-click on the link and then choose 'save as' from the menu.</b>


{{< svg "win_logo.svg" "60px" >}}
<a href="http://www2.olex2.org/olex2-distro/olex2-installer.exe" download>Download Olex2 (all versions) for Windows</a>
<i class="fas fa-download"></i>

<br>

##  MAC OS

For Mac OS we provide one DMG installer of the current version Olex2-1.3, which will automatically update. There is also the option of downloading the relevant zip files and installing Olex2 manually.

|{{< svg "MacOS_logo.svg" "60px" >}}| |
| :--: |--|
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/olex2-1.3.app.dmg" download>Download the **Olex2-1.3** installer file for Mac-OS 64 Bit</a> |
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3/olex2-mac64-intel.zip" download>Download the **Olex2-1.3** zip file for Mac-OS 64 Bit</a> |
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3-beta/olex2-mac64-intel.zip" download>Download the **Olex2-1.3 beta** zip archive for Mac-OS 64 Bit</a> |
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3-alpha/olex2-mac64-intel.zip" download>Download the **Olex2-1.3 alpha** zip for Mac-OS 64 Bit</a> |

<br>

##  Linux

We do not provide installers for Linux -- and provide zip files instead.

|{{< svg "linux_logo.svg" "60px" >}}| |
| :--: |--|
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3/olex2-linux64.zip" download>Download the **Olex2-1.3** zip file for Linux 64 Bit</a> |
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3-beta/olex2-linux64.zip" download>Download the **Olex2-1.3-beta** zip file for Linux 64 Bit</a> |
|<i class="fas fa-download"></i>|<a href="http://www2.olex2.org/olex2-distro/1.3-alpha/olex2-linux64.zip" download>Download the **Olex2-1.3-alpha** zip file for Linux 64 Bit</a> |
