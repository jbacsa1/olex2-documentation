---
title: "Olex2 Versions"
weight: 15
linkTitle: "Versions"

description: >
  What versions of Olex2 are there and which one suits me?

categories:
 - Installation

tags:
 - versions
  
---

There is always one official release version of Olex2. This is the version that you probably want to install -- and using the standard installation methods in their default mode, this is the version you will automatically get.

Olex2 will also automatically update itself within each major version.

## Olex2 major versions

Currently, the official release version of Olex2 is **Olex2-1.3**. Previously we had other versions leading up to this, but they have no longer any relevance. If you are still using **Olex2-1.2**, please update Olex2 to the latest version. This must be done manually -- there is no update path between major versions.

## Olex2 release cycle

We maintain a fairly simple release cycle for Olex2. When we make changes, we will release an **alpha** version. This will be tested by a relatively small number of users, and once that version is stable, we will 'upgrade' this version to the **beta**. At that point, the **alpha** and the **beta** are identical. Now ther is a larger group of people using this and we listen to feedback and fix what we can before we make that version the official **RELEASE** version.

{{< svg "alpha-beta-release.svg" "150px" >}}

So, once a new release has been made, all versions within the major release are the same.


>OLEX2 **ALL** versions of Olex2 are independent of each other. You can install them all, and they will not interfere with each other.

