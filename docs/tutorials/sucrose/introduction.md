---
title: "The Aim"
linkTitle: "The Aim"
weight: 1
description: "Get going with Olex2 by solving your first structure!"
---
 
![ Connectivity diagram of Sucrose](../sucrose_diagram.png)

One of the sample structures supplied with Olex2 is sucrose, so you have everything you need to get started included in the software.

In this tutorial 

