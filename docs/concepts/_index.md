---
title: "Basic Concepts"
linkTitle: "Basic Concepts"
weight: 4
description: >
  Basic concepts relevant to X-ray structure determination
---


This part contains an outline of the overall process of obtaining a crystal structure.


{{< svg "structureflow.svg" >}}
