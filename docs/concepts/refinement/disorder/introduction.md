---
title: "Disorder!"
linkTitle: "Disorder!"
weight: 1
description: "How to deal with Disorder"
---
 
Many routine crystal structures can be solved and refined with relative ease. When disorder enters the picture, the task can quickly become very tricky. Olex2 offers many tools that make working with disorder easier.
  