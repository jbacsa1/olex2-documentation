---
title: "Constraints and Restraints"
linkTitle: "Constraints and Restraints"
weight: 7
description: "Commands relating to the application of constraints and restraints"
---
 
>A atom types (to add for the selected atom) [-EADP] [-lo]

>B Makes the selected site be shared by atoms of different types.

>C
 * **-EADP**: adds the equivalent ADPs command for all atoms sharing one site.
 * **-lo**: links the occupancy of the atoms sharing the site through a free variable.


## eadp

>A atoms

>B Makes the ADP of the specified atoms equivalent.


## sadi

>A atoms or bonds [esd=0.02]

>B For selected bonds or atom pairs, SADI makes the distances specified by selecting bonds or atom pairs similar within the esd.
If only one atom is selected, it is considered to belong to a regular molecule (like PF\low{6}) and adds similarity restraints for P--F and F--F distances.
For three selected atoms (A1,A2,A3) it creates similarity restraints for A1--A2 and A2--A3 distances.



## dfix

>A *d* atom pairs or pairwise selection in order [esd=0.02]

>B For selected bonds or atom pairs, DFIX will generate length fixing restraint with the given esd.
If only one atom is selected, all outgoing bonds of that atom will be fixed to the given length with provided esd. For three selected atoms (A1, A2, A3) the A1--A2 and A2--A3 restraints will be generated.



## dang

>A *d* atom pairs or pairwise selection in order [esd=0.04]

>B For selected bonds or atom pairs, distance restraints similar to DFIX will be generated.



## tria

>A d1 d2 angle [esd=0.02]

>B For a given set of bond pairs, sharing an atom or atom triplet generates two `DFIX` commands and one `DANG` command.

>D `CODE tria 1 1 180 C1 C2 C3` will generate `DFIX 1 0.02 C1 C2 C2 C3`.
\par
`CODE DANG 2 0.04 C1 C3` will calculate the distance for DANG from d1 d2 and the angle.


## rrings

>A [d=1.39] [esd=0.01] ring_content or selection

>B Finds rings using the selection or rings content (like C6). It also sets DFIX restraint for the bond lengths using the d parameter and FLAT with e.s.d. of 0.1 restraint for the ring. It also adds SADI restraints for the 1-3 distances. If d is negative, the SADI restraint is used instead.


## flat

>A [atoms] [esd=0.1]

>B Restrains given fragment to be flat (can be used on the grown structure) within given esd.


## chiv

>A [atoms] [val=0] [esd=0.1]

>B Restrains the chiral volume of the provided group to be within given esd.


## simu

>A [d=1.7] [esd12=0.04] [esd13=0.08]

>B Restrains the ADPs of all 1--2 and 1--3 pairs within the list of given atoms to be similar, taking the provided esd into account.


## delu

>A [esd12=0.01] [esd13=0.01]

>B Rigid Bond restraint


## isor

>A [esd=0.1] [esd_terminal=0.2]

>B Restrains the ADP of the given atom(s) to be approximately isotropic.


## same

>A *n*

>B Splits the selected atoms into the *n* groups and applies the SAME restraint to them. Olex2 will manage the order of atoms within the .ins file, however mixing rigid group constraints and the SAME instructions might lead to an erroneous instruction file. Note that if only two atoms are selected in two fragments with identical connectivity, Olex2 employs the matching procedure and sets SAME for the two fragments to which the atoms belong.


## showp

>A [any]; space separated part number(s)

>B Shows only the parts requested: `CODE showp 0 1` will show parts 0 and 1, `CODE showp 0` just part 0. `CODE showp` by itself will display all parts.


## split

>A [-r={eadp, isor, simu}]

>B Splits selected atom(s) along the longest ADP axis into two groups and links their occupancy through a free variable.

>C
 * **-r**: adds specific restraints/constraints (EADP, ISOR or SIMU) for the generated atoms


## afix

>A number{mn}[-n]

>B If no atoms are provided and afix corresponds to a fitted group where *n* is 6 or 9 (such as 106 or 79), all the rings which satisfy the given afix will be automatically made rigid (this is useful in the case of many PPh3 fragments). Alternatively a single ring atom can be selected to make that ring rigid. In other cases, depending on afix, either 5,6 or 10 atoms will be expected. In special cases of afix, 0, 1 and 2 can be used to remove afix, fix all parameters or leave just the coordinates refinable. All other afix instructions will consider the first atom as a pivot atom and the rest as dependent atoms.
The AFIX command can also be used to generate missing atoms to complete rings or fragments. For example, the following command generates three missing atoms in positions 4,5 and 6 for the Ph ring when applied to a selection of 3 atoms (assumed to be in positions 1, 2 and 3):
`CODE AFIX 66 1,2,3`
Note that there are no white spaces between the identification of the selected positions.

>C
 * **-n**: consider *n*-atoms as parts of rings


## part

>A [part=new_part] [occupancy] [atoms] [-p=1]

>B Changes the part number/occupancy for selected atom

>C
 * **-lo**: links occupancies of the atoms through a +/-variable or linear equation (SUMP) depending on the -p[=1].
 * **-p**:  specifies how many parts to create. If -p=1, -lo is ignored and the given or new part is assigned to the provided atoms. If the number of parts is greater than 2 and -lo option is given, a new SUMP restrain will be automatically added.


## fvar

>A [part=new_part] [occupancy] [atoms] [-p=1]

>B This command links two or more atoms through a free variable.
If no atoms are given, the current free variables are printed.
If no value is given but two atom names are give, the occupancies of those atoms are linked through a new free variable.
If a value of 0 is given, the occupancy of the specified atoms will be refined freely.
If the value is not 0, the occupancy value of the specified atoms is set to the given value.


## sump

>A [val=1] [esd=0.01] [part=new_part] [occupancy] [atoms] [-p=1]

>B Creates a new linear equation. If any of the selected atoms has refinable or fixed occupancy, a new variable is added with the value 1/(number of given atoms). Otherwise, an already used variable is used with weight of 1.0. Also look at `CODE part` command.

>D If 3 atoms (A1, A2, A3) are selected, this command will generate three free variables (var1, var2 and var3) and insert the `1 1 var1 1 var2 1 var3` instruction (equivalent to

$1 = 1 \times occ(A1) + 1 \times occ(A2) + 1 \times occ(A3)$


## mode split

>A [-r={eadp, isor, simu}]

>B Splits subsequently clicked atoms into parts. While in this mode, the newly generated atoms can be selected and moved as a group with SHIFT key down, or rotated when dragging the selection. The original and generated atoms will be placed into different parts.

>C
 * **-r**: can be used to generate extra restraints or constraints for original and generated atoms (see also the *split* command); values EADP, ISOR or SIMU are allowed


## mode fit

>A [-s]

>B Allows fitting of selected group (moving and rotating in 3D).

>C
 * **-s**: a new group is created at the fitted location. The occupancy of this and the original group is constrained to be 1.


## ImportFrag

>A [-a] [-d] [-p]

>B Import and .xyz file into the current model. Mode fit is automatically executed to help with fitting the imported molecule.

>C
 * **-a**: sets AFIX to the imported molecule
 * **-d**: generates DFIX for 1,2 and 1,3 distance for the imported molecule
 * **-p**: sets given part to the imported molecule


## restrain

>A ADP [Ueq] {Ueq, volume}1

>B This is a generic macro to generate restraints. By default, the Ueq/volume similarity restraint is generated. If the ADP Ueq/volume is unknown then the restraint value will be randomly generated. 

>A Bond [d atoms]

>A Angle value [atoms]
 
>A Dihedral value [atoms]

>E Only available in Olex2.refine


## constrain

>A U [atoms]
>A Site or xyz [atoms]
>A Same group [n=2 atoms]

>B This is a generic macro to generate constraints.  

The same group or non-crystallographic symmetry constraint makes two or more groups identical and linked through a transformation matrix, refined as a shift and 3 Euler angles. If two atoms are given, they must belong to two identical fragments; Olex2 will then try to match the fragments containing the atoms and automatically generate the constraint. In more generic/complex cases the user has to provide the number of groups to generate the constraint for and also the selection which matches atoms in the fragments.


## xf.rm.ShareADP1

>A [atoms]

>B Generates the shared, rotated ADP constraint. For 3 atoms, it generates an ADP rotated around the bond e.g. around X-C bond in X-CF\low{3}. For more atoms, it creates an ADP rotated around the normal of the plane formed by the atoms.
  