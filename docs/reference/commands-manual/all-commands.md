---
title: "All commands"
linkTitle: "All commands"
weight: 13
description: "All documented commands"
---
 
>A A1, A2 or atoms

>B Adds a bond to the connectivity list for the specified atoms. This operation will also be successful if symmetry equivalent atoms are specified.


## ads

>A {elp, sph, ort, std}

>B A function for drawing styles development. Changes atom drawing style for all/selected atoms.

>C
 * **elp**: represents atoms as ellipsoids (if ellipsoids are available)
 * **sph**: represents atoms as spheres
 * **ort**: same as elp, but spheres have one of the quadrants cut out
 * **std**: a stand-alone atom (i.e. shown as a cross, in wire-frame mode)


## afix

>A number{mn}[-n]

>B If no atoms are provided and afix corresponds to a fitted group where *n* is 6 or 9 (such as 106 or 79), all the rings which satisfy the given afix will be automatically made rigid (this is useful in the case of many PPh3 fragments). Alternatively a single ring atom can be selected to make that ring rigid. In other cases, depending on afix, either 5,6 or 10 atoms will be expected. In special cases of afix, 0, 1 and 2 can be used to remove afix, fix all parameters or leave just the coordinates refinable. All other afix instructions will consider the first atom as a pivot atom and the rest as dependent atoms.
The AFIX command can also be used to generate missing atoms to complete rings or fragments. For example, the following command generates three missing atoms in positions 4,5 and 6 for the Ph ring when applied to a selection of 3 atoms (assumed to be in positions 1, 2 and 3):
`CODE AFIX 66 1,2,3`
Note that there are no white spaces between the identification of the selected positions.

>C
 * **-n**: consider *n*-atoms as parts of rings


## arad

>A {sfil, pers, isot, isoth, bond, vdw}

>B A function for drawing styles development - applies different radii to all/selected atoms.

>C
 * **sfil**: sphere packing radii (as in ShelXTL XP)
 * **pers**: a fixed radii for model viewing
 * **isot**: each atom has its own radius depending on the value of the Uiso or ADP
 * **isoth**: same as isot, but the H atoms are also displayed with their real Uiso's
 * **bond**: all atoms get the same radii as default bond radius
 * **vdw**: the default/loaded Van der Waals radii used in most of the calculations


## azoom

>A % [atoms]

>B Changes the radii of all/given atoms, the change is given as a percentage.


## brad

>A *r* [hbonds]

>B Adjusts the bond radii in the display. If *hbonds* is provided as the second argument, the given radius *r* is applied to all hydrogen bonds. This operates on all or selected bonds.


## calcfourier

>A {-calc,- diff, -obs, -tomc} [-r=0.25 ANGST] [-i] [-scale=simple] [-fcf]

>B Calculates Fourier for current model.

>C
 * **-r**: the resulting map resolution in ANGST
 * **-i**: integrates the calculated map
 * **-scale**: when Olex2 calculates structure factors, it uses the linear scale as a $sum(F_o^2)/sum(F_c^2)$ by default, however a linear regression scale can be also used (use -scale=regression)
 * **-fcf**: Olex2 will use an FCF with LIST 3 structure factors as a source of the structure factors. If this option is not specified, Olex2 will calculate the structure factors using the the reflection used in the refinement (use the `CODE hklstat` command to see more information on reflections).


## calcvoid

>A [radii file name] [all atoms/selected atoms] [-d=0] [-p] [-r=0.2 ANGST]

>B Calculates and displays the structure map. Also calculates the largest channels along crystallographic directions and the packing index. 

>C
 * **-d**: extra distance from the surface (added to the atomic radii)
 * **-p**: precise calculation where each map voxel is tested. The default quick algorithm uses the atom masks to find the volume occupied by the molecule. The precise calculation is vectorised.
 * **-r**: resolution, a resolution of at least 0.1 ANGST and -p option is required to get values for publishing
Note: The radii used in the calculation are currently coming from the CSD website:
 * *http://www.ccdc.cam.ac.uk/products/csd/radii*
However there are several ways how the radii can be changed, one of which is to provide  a file name with radii ([element radius] a line format). The other is to load the radii from the same kind of the file using `CODE load radii vdw` command.


## chiv

>A [atoms] [val=0] [esd=0.1]

>B Restrains the chiral volume of the provided group to be within given esd.


## compaq

>A [-a] [-c] [-q] [-m]

>B Moves all atoms or fragments of the asymmetric unit as close to each other as possible. If no options are provided, all fragments are assembled around the largest one.

>C
 * **-a**: assembles broken fragments
 * **-c**: similar to the default behaviour but considers atom-to-atom distances. It will move all atoms as close as possible to the largest fragment in the structure.
 * **-q**: moves the electron density peaks closer to the atoms.
 * **-m**: disconnects metals, then does compaq -a and then reattaches the metals.


## conn

>A n [r] atoms

>B Sets the maximum number of bonds for the specified atoms to n and changes the default bond radius for the given atom type to r.

>D `CODE conn 5 \$C` sets the maximum number of bonds all C atoms can have to 5
`CODE conn 1.3 \$C` changes the bonding radius for C atoms to 1.3 (the floating point is used to distinguish between n and r in this case).
`CODE conn 5 1.3 \$C` combines the two commands above


## constrain

>A U [atoms]
>A Site or xyz [atoms]
>A Same group [n=2 atoms]

>B This is a generic macro to generate constraints.  

The same group or non-crystallographic symmetry constraint makes two or more groups identical and linked through a transformation matrix, refined as a shift and 3 Euler angles. If two atoms are given, they must belong to two identical fragments; Olex2 will then try to match the fragments containing the atoms and automatically generate the constraint. In more generic/complex cases the user has to provide the number of groups to generate the constraint for and also the selection which matches atoms in the fragments.


## dang

>A *d* atom pairs or pairwise selection in order [esd=0.04]

>B For selected bonds or atom pairs, distance restraints similar to DFIX will be generated.



## delbond

>A A1, A2 or Selected bond(s)

>B Removes selected bonds from the connectivity list. Use this command to permanently remove bonds from the display too.


## delu

>A [esd12=0.01] [esd13=0.01]

>B Rigid Bond restraint


## dfix

>A *d* atom pairs or pairwise selection in order [esd=0.02]

>B For selected bonds or atom pairs, DFIX will generate length fixing restraint with the given esd.
If only one atom is selected, all outgoing bonds of that atom will be fixed to the given length with provided esd. For three selected atoms (A1, A2, A3) the A1--A2 and A2--A3 restraints will be generated.



## direction

>B The command prints current normal in crystallographic coordinates and tries to match it to a crystallographic direction.



## eadp

>A atoms

>B Makes the ADP of the specified atoms equivalent.


## editmaterial

>A {helpcmd, helptxt, execout, error, exception, any object name available with lstgo}

>B Brings up a dialog to change properties of the specified text section or graphical object.

>C
 * **helpcmd**: the command name in the help window
 * **helptxt**: the body of the help item
 * **execout**: the output text printed in the console of external programs
 * **error**: reporting errors in the console
 * **exception**: reporting exception in the console

This command can be used to edit properties of any objects printed by `CODE lstgo` macro. An example of that could be editing material of the console text:

>D `CODE EditMaterial Console`. The the object name is case sensitive.


## envi

>A [r=2.7 ANGST] A1 or  selected atom [-h] [-q]

>B Prints a list of those atoms within a sphere of radius r around the specified atom. If more than one atom is selected, the one that was selected first is used.

>C
 * **-h**: adds hydrogen atoms to the list
 * **-q**: adds Q-peaks to the list


## exyz

>A atom types (to add for the selected atom) [-EADP] [-lo]

>B Makes the selected site be shared by atoms of different types.

>C
 * **-EADP**: adds the equivalent ADPs command for all atoms sharing one site.
 * **-lo**: links the occupancy of the atoms sharing the site through a free variable.


## file

>A file

>B Saves current model to a file. By default an ins file is saved and loaded. It is also possible to save the current file in cif,ins,p4p,mol,mol2,pdb,xyz formats.

## fix

>A {occu, xyz, Uiso} [atoms]

>B Fixes the specified refinement parameter, i.e. these parameters will not be refined in subsequent refinement cycles.

>C
 * **-occu**: will fix the occupancy 
 * **-xyz**: will fix the xyz coordinates
 * **-Uiso**: will fix the whole ADP

>D `CODE fix occu 0.5`: will set and fix the occupancy of the current selection to 0.5
`CODE fix xyz`: will fix the x, y and z co-ordinates of the currently selected atoms (i.e. not refine them).


## FixHL

>B Updates H-atom labels according to the labels of the bearing atoms


## flat

>A [atoms] [esd=0.1]

>B Restrains given fragment to be flat (can be used on the grown structure) within given esd.


## free

>A {occu, xyz, Uiso} [atoms]

>B The opposite of FIX - makes the specified parameters for the given atoms refineable. Freeing the occupancy is also available from the context menu.


\chapter{Selected Commands}
> Selected Olex2 commands. 

There are hudreds of commands in Olex2. This is a selection of those, that are most important for working with Olex2.

## fvar

>A [part=new_part] [occupancy] [atoms] [-p=1]

>B This command links two or more atoms through a free variable.
If no atoms are given, the current free variables are printed.
If no value is given but two atom names are give, the occupancies of those atoms are linked through a new free variable.
If a value of 0 is given, the occupancy of the specified atoms will be refined freely.
If the value is not 0, the occupancy value of the specified atoms is set to the given value.


## grad

>A [C1 C2 C3 C4] [-p]

>B Choose the colour of the four corners of the graduated background. 

>C
 * **-p**: a file name for the picture to be placed in the background


## grow

>A [atoms] [-w] [-s]

>B Grows all possible/given atoms. For polymeric structures or structures that require to be grown several times, Olex2 will continue grow until the operation results in a symmetry element that has been used previously.

>C
 * *-w**: permits the application of previously used symmetry operations to other fragments of the asymmetric unit. In other words: if parts of the structure have been grown, this command will also generate symmetry equivalent atoms that are not connected to the already grown fragment, i.e. solvent molecules.

>D If the main molecule is grown, but only one solvent molecule is shown, using `CODE grow -w` will produce other solvent molecules using symmetry operators used to grow the main molecule.


## hklappend

>A -h=h1;h2;.. -k=k1;k2.. -l=l1;l2..

>B Acts in the opposite way to `CODE excludehkl`.


## hkledit

>A [h k l]

>B Brings up a dialogue, where "bad" reflections from the Shelx lst file and all its constituent symmetry equivalents can be inspected and flagged to be excluded from the refinement.
In contrast to the `CODE OMIT h k l` instruction, which  excludes the reflection and all it equivalents, this dialogue allows the exclusion of those equivalents that are actually outliers.
If a particular reflection is specified, this particular reflection and all its constituent equivalents can be viewed.


## hklexclude

>A [-h=h1;h2;.. -k=k1;k2.. -l=l1;l2.. [-c]

>B This function provides a mechanism to reversibly exclude some reflections from refinement (these reflections will be moved to the end of the .hkl file so they appear after the `0 0 0` reflection).

>C
 * **-c**: option controls how the given indices are treated. If no **-c** option is provided, then any reflection having any of the given *h*, *k* or *l* indices will be excluded; otherwise only reflections with indices within provided *h*, *k* and *l* will be excluded.


## hklstat

>B Prints detailed information about reflections used in the refinement.



## hklview

>A [-h=h1;h2;.. -k=k1;k2.. -l=l1;l2.. [-c]

>B Shows the reflection currently used in the refinement (use CTRL+T a few times to centre on the reflection view).



## htab

>A [minimal angle=150$^\circ$] [maximum bond length 2.9 ANGST] [-t] [-g]

>B Searches and adds found hydrogen bonds (like HTAB and RTAB in Shelx) into a list for the refinement program to add to the CIF. Equivalent symmetry positions are automatically inserted and merged with the existing ones. The command can be executed several times with different parameter values, only one unique instruction will be added. 

>C
 * **-t**: adds extra elements (comma separated like in -t=Se,I) to the donor list. Defaults are [N,O,F,Cl,S,Br]
 * **-g**: if any of the found bonds are generated by symmetry transformations, the structure is grown using those symmetry transformation


## ImportFrag

>A [-a] [-d] [-p]

>B Import and .xyz file into the current model. Mode fit is automatically executed to help with fitting the imported molecule.

>C
 * **-a**: sets AFIX to the imported molecule
 * **-d**: generates DFIX for 1,2 and 1,3 distance for the imported molecule
 * **-p**: sets given part to the imported molecule


## isor

>A [esd=0.1] [esd_terminal=0.2]

>B Restrains the ADP of the given atom(s) to be approximately isotropic.


## kill

>A Atom names or selection or `CODE labels`

>B Deletes given or selected atoms, bonds or labels. Note that if the bonds are deleted this way they will reappear the next time the structure connectivity is updated. Use `CODE delbond` to remove bonds permanently.


## label

>A [atoms]

>B Adds labels to all/given/selected atoms and bonds. These labels can be moved by pressing the SHIFT key while holding down the LEFT MOUSE button, and edited by double-clicking on them.

>C
 * **-type**: {subscript, brackets, default
 * **-symm**: {[$], ## , full} - if an atom is generated by non-identity symmetry operation, it will be added as a superscript. Note that if ##  is used as the symmetry identifier then the labels of every type of new atom generated (in growing or packing) should be recalculated. Olex2 will then print current mapping of the symmetry numbers to the symmetry operators.


## load

>A {scene, style, view, model, radii} [file_name]

>B Load one of the previously saved items. If no file name is provided, the 'Open file...' dialog will appear. If just a file name is provided (the extension will be guessed by Olex2); for styles and scene the last used folders will be used by default, the current folder will be used for the views and models. Loading radii (vdw, pers, sfil) allows the user to change the radii Olex2 uses for various calculations/display.


## lstsymm

>B Prints symmetry operations and their codes for the current structure.


## match

>A [atoms] [-a] [-w] [-i] [-n] [-u] [-esd] [-h] [-cm] [-o]

>B This procedure finds the relationship between the connectivity graphs of molecular fragments of loaded structure and aligns the fragments. If no arguments are given, the procedure analyses all fragments and in the case when fragments with matching connectivity are found, it aligns Acta A45 (1989), 208 and prints corresponding root mean square distance (RMSD) in angstroms. If two atoms are provided (explicitly by name or through the selection) the graph relation information - orientation matrix and the matching atoms - is printed. Use `CODE -a` option to align the fragments.

>C
 * **-a**: align the fragments (used when a pair of atoms are provided)
 * **-w**: specifies weight for the atomic positions - by default the unit weights are used. If this option is given, the atomic position are weighted by the element mass. 
 * **-i**: try to invert one of the fragments.
 * **-n**: transfers labels from one fragment to another (two atoms should be provided as 'to' and 'from' fragments). If the value is a symbol (or a set thereof) this is appended to the label. `CODE $xx` replaces the symbol after the atom type symbol with xx, leaving the ending. `CODE -xx` changes the ending of the label TO xx. Note that if the molecules match with `CODE -i` options, this should also be provided for the label transfer.
 * **-u**: restores the coordinates of the matched fragments - this is useful if the grown structure is matched.
 * **-esd**: if the variance-covariance matrix can be located (after refinement with the MORE negative option in the xl), the esd on the RMSD can be calculated using this option.
 * **-h**: calculates the final matching and RMSD calculation without taking H-atoms into account.
 * **-o**: when overlaying molecules from different structures whole lattices (if packed/grown) are overlayed, not only the two fragments. To use, select an atom in a fragment of one lattice and an atom in a matching fragment of the other lattice.
When a selection of two atoms is given the command prints the alignment matrix. This matrix alongside the `CODE sgen` command can be used to generate new atoms. Use the `CODE -cm` option to copy the matrix to the clipboard.


## matr

>A [1,2,3 or abc] or [abc a1b1c1] or [x11 x12 x13 y11 y12 y13 z11 z12 z13]

>B Orientates the model along; a (1 or `1 0 0`), b (2 or `0 1 0`), c (3 or `0 0 1`) or any other crystallographic direction e.g. `123`, which sets the current normal along the  $(1 \times a+2 \times b+3 \times c)$ vector. Two crystallographic directions (from and to) may be specified to align the current view normal along the (to-from) vector. Also a full Cartesian matrix can be specified. If the directions are signed or consist of multiple digits, all components should be of the same length like in `1 2 0 1 0 1` or `-1 +1 +1` (same as `-1 0 1 0 1`). If no arguments are given, the current Cartesian orientation matrix is printed.

>C
 * **-r**: uses the reciprocal lattice instead of the direct.

>D `CODE matr 1` or `CODE matr a` or `CODE matr 100` - sets the current normal along the crystallographic a direction.
`CODE matr 100 011` sets the current normal along the (`0 1 1 -- 1 0 0`) direction (the normal direction changes if from and to are swapped)


## mode

>B If Olex2 is in a Mode, the chosen action will be applied to all subsequently clicked atoms. The mouse pointer will change from the default arrow symbol to signify that Olex2 is in a mode. To get out of a mode, simply press the ESC key or press the link in the orange "mode" box.


## mode fit

>A [-s]

>B Allows fitting of selected group (moving and rotating in 3D).

>C
 * **-s**: a new group is created at the fitted location. The occupancy of this and the original group is constrained to be 1.


## mode fixu

>B Fixes Uiso or ADP for subsequently clicked atoms.



## mode fixxyx

>B Fixes the coordinates for subsequently clicked atoms.


## mode grow 

>A [-s] [-v] [-b] [-shells]

>B Displays the directions in which the molecule can be grown.

>C
 * **-s**: also shows the short interaction directions.
 * **-v**: [2.0 ANGST] shows directions to the molecules within the v value of the Van der Waals radii of the selected atoms, which can be generated by clicking on the direction representations. Only unique symmetry operations (producing shortest contacts) are displayed. If an atom is selected before entering this mode, the environment of only this atom(s) can be grown.
 * **-r**: shows "growing bonds" to symmetry equivalent atoms of the selected one(s) within 15 ANGST. Shortcut CTRL+G is used to enter the 'mode grow'
 * **-shells**: only applicable in `CODE mode grow -shells` - allows growing atom by atom. If a 'grow' bond is clicked, only the immediate attached to that bond atom is grow, if the atom with outgoing 'grow' bonds is clicked - atoms for all bonds are grown


## mode name

>A [-p] [-s] [-t] [-a=0]

>B Puts the program into the naming mode.

>C
 * **-p**: label prefix
 * **-s**: label suffix
 * **-t**: element symbol
 * **-a**: autocomplete, turned off by default. Value 1 switches the autocompleting on, with value 2 stopping the procedure when an atom of a different type is encountered on the way. Value 4 - when an atom with a different part is encountered on the way and value 6 is selected when combining the cases of 2 and 4. A special value, 8, does automatic naming.


## mode occu

>A occupancy_to_set

>B Sets atom occupancy to the provided value for subsequently clicked atoms.


## mode pack

>B Displays the position of symmetry equivalent asymmetric units as tetrahedra. These asymmetric units can be generated by clicking on the corresponding tetrahedron.


## mode split

>A [-r={eadp, isor, simu}]

>B Splits subsequently clicked atoms into parts. While in this mode, the newly generated atoms can be selected and moved as a group with SHIFT key down, or rotated when dragging the selection. The original and generated atoms will be placed into different parts.

>C
 * **-r**: can be used to generate extra restraints or constraints for original and generated atoms (see also the *split* command); values EADP, ISOR or SIMU are allowed


## molinfo

>A [radii file name] [atoms] [-g=5] [-s=o]

>B Calculates molecular volume and surface area for all/selected atoms.

>C
 * **-g**: generation of the triangulation process
 * **-s**: source of the triangles for the sphere triangulation, [o]ctahedron or [t]etrahedron are available. Generation 5; for octahedron-approximate sphere by 8192 triangles, for tetrahedron by 4096 triangles. Each generation up increases the number of triangles by factor of 4, generation down - decreases by the same factor.


## mpln

>A [atoms] [-n] [-r] [-rings]

>B Finds the best plane through the current selection or given atoms, or out of all visible atoms if none are given.

>C
 * **-n**: sets the view along the normal of the plane.
 * **-r**: creates a regular plane.
 * **-rings**: creates planes for all rings given by a template like NC5.


## name

>A [selection/atom names] [-c] [-s=]

>B The command allows the atom names to be changed.

>C
 * **-c**: checks if the generated names are unique.
 * **-s**: changes the suffix only (no value removes the suffix, i.e. the part of the label after the element symbol and numerical value)

>D `CODE name O1 O2`: renames O1 to O2
`CODE name 1`: (some atoms selected). Sequentially names the atoms in order of the selection    by adding 1,2, etc to the element symbol. Note that in this case if any generated name is not unique (and the -c option is not given), a random name will be generated.
`CODE name $q C`: changes the element type of Q to C - all the electron density peaks will become carbons.
`CODE name sel -s=a`: changes suffix of the selected atoms to 'a', replacing any existing suffix. Note that sel is a required keyword in this case (but may be removed in the future.
`CODE name Q? C?`: changes the type for all electron density peaks with single number labels to carbon atoms, preserving the number




## omit

>A h k l

>B Inserts `CODE OMIT h k l` instruction in the ins file. Use `CODE delIns omit` to remove all the OMITs from the INS file header.

>A `CODE omit val`

>B Inserts `OMIT h k l` for all reflections with $|{F_{o}}^2 - {F_{c}}^2| > val$

>A omit s 2theta

>B Inserts `CODE OMIT s 2theta` instruction in the ins file


## pack

>A a_from a_to b_from b_to c_from c_to [atoms]

>B Packs all or specified atoms within given dimensions.

>C
 * **-c**: prevents clearing existing model.

>D `CODE pack $O` will pack all O atoms with the default of -1.5 to 1.5 cells range.

>A from to

>B Equivalent to `pack from to from to from to`, like `CODE pack 0 1` is expanded to `pack 0 1 0 1 0 1`

>A Cell

>B Shows content of the unit cell. In conjunction with `CODE grow -w`, it allows the creation of views where all asymmetric units contributing to the unit cell are shown.

>A Wbox

>B Packs the volume of the structure inside a 3D selection box. You can select 3 atoms and type `CODE sel wbox` to create a box around just that part of the structure. To keep already shown box around atoms and work on another part of the structure, use the $-c$ option.

>A r

>B Packs fragments within radius r of the selected atom(s) or the centre of gravity of the asymmetric unit.


## part

>A [part=new_part] [occupancy] [atoms] [-p=1]

>B Changes the part number/occupancy for selected atom

>C
 * **-lo**: links occupancies of the atoms through a +/-variable or linear equation (SUMP) depending on the -p[=1].
 * **-p**:  specifies how many parts to create. If -p=1, -lo is ignored and the given or new part is assigned to the provided atoms. If the number of parts is greater than 2 and -lo option is given, a new SUMP restrain will be automatically added.


## pict

>A filename.ext [n=2] [-pq] [-dpi] 

>B Generates a bitmap image of what is visible on the molecule display. n refers to the size of the output image. If n<10, it refers to a multiple of the current display size. If `n>100`, it refers to the width of the image in pixels. 
ext {png, jpg, bmp}. png is best. 

>C
 * **-pq**: print quality.
 * **-nbg**: removes the background from the picture (making it transparent with the alpha channel).
 * **-dpi**: physical resolution of the image.


## picta

>A filename.ext [n=1] [-pq] [-dpi]

>B A portable version of `CODE pict` with limited resolution (see explanation for *n* in `CODE Pict`), which is OS and graphics card dependent. This function will also use the graphics card settings like antialiasing when producing the picture. 

>C
 * **-pq**: print quality 
 * **-nbg**
 * **-dpi**: same as for `CODE pict`


## pictpr

>A filename

>B Creates PovRay file for current view


## pictps

>A filename.ps

>B Generates a post-script file of what is visible in the molecule display. The bond width is taken from the display. This can be changed with `CODE brad`.

>C
 * **-atom_outline_color**: the colour of the atom outline, used for extra 3D effect for the intersecting objects [0xFFFFFF]
 *  * **-atom_outline_oversize**: the size of the outline [5]%
 * **-bond_outline_color**: same as for the atom, can be changed to black to highlight bond boundaries 
 * **-bond_outline_oversize**: the size of the outline [10]%
 * **-color_fill**: Fills the ellipses with colour.
 * **-color_bond**: Bonds will be in colour.
 * **-color_line**: Lines representing the ellipses will be in colour.
 * **-div_pie**: number of stripes in the octant [4]
 * **-lw_ellipse**: line width [0.5] of the ellipse
 * **-lw_font**: line width [1] for the vector font
 * **-lw_octant**: line width [0.5] of the octant arcs
 * **-lw_pie**: line width [0.5] of the octant stripes
 * **-p**: perspective
 * **-scale_hb**: scale for H-bonds [0.5]


## picts

>A filename.ext [n=1] [-a=6] [-s=10] [$-h=n \times (screen height)$]

>B Creates a 'stereo' picture with two views taken with the +/- a option. Value is rotated around y axis and placed into one picture separated by s % of a single projection width.

>C
 * **-a**: half of the view angle
 * **-s**: separator width in %
 * **-h**: the height of the output, by default equal to current screen height multiplied by the given resolution.


## pim

>B Helps with creating pictures when metal - \pi interactions need special drawing (e.g. in the case of Cp-Me, a single bond will be rendered from the ring centroid to the metal vs 5 bonds from every C atom to the metal). Note that currently, these stippled bonds do not appear in the PostScript rendered pictures and thus for PS pictures, a workaround with creating centroids is needed. To remove all of the lines created by this command - right click on one of them and choose Graphics->Select the group, then hit the DEL key.


## pipi

>A [centroid-to-centroid distance 4 ANGST] [centroid-to-centroid shift 3 ANGST] [-g] [-r=C6,NC5]

>B The command analyses the \pi-\pi interactions (only stacking interactions for now) for flat regular C6 or NC5 rings and prints information for the ones where the intercentroid distance is smaller than [4] ANGST and the intercentroid shift is smaller than [3] ANGST.

>C
 * **-g**: if any of the rings is fully or partially constructed of symmetry generated atoms, it grows the structure using those symmetry operators.
 * **-r**: ring content, the defaults are C6 and NC5 rings, the rings are tested for being flat and regular.


## restrain

>A ADP [Ueq] {Ueq, volume}1

>B This is a generic macro to generate restraints. By default, the Ueq/volume similarity restraint is generated. If the ADP Ueq/volume is unknown then the restraint value will be randomly generated. 

>A Bond [d atoms]

>A Angle value [atoms]
 
>A Dihedral value [atoms]

>E Only available in Olex2.refine


## rota

>A [axis angle] or [x y z angle increment]

>B Changes current view by rotating around given axis (x, y or z) when two arguments are provided. It makes a continuous rotation around give axis when 5 arguments are provided. Note that the X axis is aligned horizontally, Y vertically and Z is out of the screen plane.

>D `CODE rota x 90` rotates the structure 90 degrees around the x axis.
`CODE rota 0 0 1 90 1` rotates the model in the screen plane (around z) 90 degrees with 1 degree increment.


## rrings

>A [d=1.39] [esd=0.01] ring_content or selection

>B Finds rings using the selection or rings content (like C6). It also sets DFIX restraint for the bond lengths using the d parameter and FLAT with e.s.d. of 0.1 restraint for the ring. It also adds SADI restraints for the 1-3 distances. If d is negative, the SADI restraint is used instead.


## sadi

>A atoms or bonds [esd=0.02]

>B For selected bonds or atom pairs, SADI makes the distances specified by selecting bonds or atom pairs similar within the esd.
If only one atom is selected, it is considered to belong to a regular molecule (like PF\low{6}) and adds similarity restraints for P--F and F--F distances.
For three selected atoms (A1,A2,A3) it creates similarity restraints for A1--A2 and A2--A3 distances.



## same

>A *n*

>B Splits the selected atoms into the *n* groups and applies the SAME restraint to them. Olex2 will manage the order of atoms within the .ins file, however mixing rigid group constraints and the SAME instructions might lead to an erroneous instruction file. Note that if only two atoms are selected in two fragments with identical connectivity, Olex2 employs the matching procedure and sets SAME for the two fragments to which the atoms belong.


## save

>A {scene, style, view, model} [file_name]

>B If the file name is not provided, the *Save as...* dialog will be shown which allows you to save current settings to file. 
The scene saves current font names/sizes as well as the materials for the specific console output e.g. external programs output, error and exception reporting.
The style saves information about the appearance of objects in the scene.
The view saves current zoom and the scene orientation.
The model saves current view including the crystallographic mode and style.


##  sel

 * **155**: all symmetry generated atoms currently shown, which were generated by the given symmetry operation.
 * **rings NC5**: all pyridine rings
 * **fvar -2**: atoms where a parameter is linked to FVAR 2
 * **part 1**: all atoms in PART 1
 * **isot**: all isotropic atoms
 * **frag C5**: the whole fragment containing C5
 * **$E**: all atoms of the given type. E is a chemical element symbol or one of the following: * - all types, M - all metals, X - all halogens
 * $*,**E**: all atoms, *but* of the E type.
 * $*,**H**: all non-H atoms
 * **wbox**: Shows the 3D selection box constructed for all/selected atoms. This box can be used to pack the structure or to crop the voids display/electron density maps. If the third argument (cell) is provided, the frame gets the dimensions of the unit cell rather than being rectangular (this box can be used for packing and 3D maps trimming/extending).
 * **ofile** *n*: slects all atoms in the overlayed file *n*; if *n* is 0, elements of the currently focused file are selected


##  sel atoms

>A where

 * **xatom.bai.mw** > 20: Select all atoms where the atomic mass is larger than 20
 * **xatom.bai.z** > 2: Select all atoms where the atomic number is greater than 2
 * `CODE atoms`: all atoms


##  sel bonds

>A where

- **xbond.length > 2**: all bonds longer than 2 ANGST
- **xbond.b.bai.z == 1**: all bonds where the lightest atoms is H
- `CODE atoms`: all atoms


## setfont

>A {Console, Picture_labels} 

>B Brings up the dialog to choose a font for the Console or Labels, which end up on the picture. Use the built in function 'choosefont([olex2])' to choose a system. Alternatively, a specially prepared/portable font can be used to specify the font.


## sgen

>A atoms

>B Generates symmetry equivalents of the selected atoms (or all atoms, if there is no selection) using the provided symmetry operation.
Note: For symmetry operations starting with `-` and a letter, a leading zero must be added or the expression has to be quoted (for example, `0-x,-y,-z`), otherwise Olex2 confuses this with an option. The Symmetry operation is represented as `1_555, 1555` or `-1+X,Y,Z` and atoms as a selection or a names list. As a special case, twelve numbers can be provided to specify any matrix operating on the fractional coordinates (e.g. see the match)


## showp

>A [any]; space separated part number(s)

>B Shows only the parts requested: `CODE showp 0 1` will show parts 0 and 1, `CODE showp 0` just part 0. `CODE showp` by itself will display all parts.


## simu

>A [d=1.7] [esd12=0.04] [esd13=0.08]

>B Restrains the ADPs of all 1--2 and 1--3 pairs within the list of given atoms to be similar, taking the provided esd into account.


## sort

>A [m] [l] [p] [h] [z] [n] [s] atoms
[s] [h] [m] moiety

>B The sorting of atoms in the atom list is very powerful but also quite complex.

>C
 * **-m**: atomic weight
 * **-z**: atomic number
 * **-l**: label, considering numbers
 * **-p**: part, 0 is first followed by all positive parts in ascending order and then negative ones
 * **-h**: to treat hydrogen atoms independent of the pivot atom.
 * **-s**: non-numerical label suffix
 * **-n**: number after the atom symbol

Sorting of moieties
 * **-s**: by size
 * **-h**: by heaviest atom
 * **-m**: by molecular weight

>D *sort [+atom_sort_type]*, *sort [Atoms]\- [moiety [+moiety_sort_type]\- [moiety_atoms]]*. If just *moiety* is provided - the atoms will be split into the moieties without sorting.

`CODE sort +m1 F2 F1 moiety +s` will sort atoms by atomic mass and label, put F1 after F2 and form moieties sorted by size. Note that when sorting atoms, any subsequent sort type operates inside the groups created by the preceding sort types.


## split

>A [-r={eadp, isor, simu}]

>B Splits selected atom(s) along the longest ADP axis into two groups and links their occupancy through a free variable.

>C
 * **-r**: adds specific restraints/constraints (EADP, ISOR or SIMU) for the generated atoms


## sump

>A [val=1] [esd=0.01] [part=new_part] [occupancy] [atoms] [-p=1]

>B Creates a new linear equation. If any of the selected atoms has refinable or fixed occupancy, a new variable is added with the value 1/(number of given atoms). Otherwise, an already used variable is used with weight of 1.0. Also look at `CODE part` command.

>D If 3 atoms (A1, A2, A3) are selected, this command will generate three free variables (var1, var2 and var3) and insert the `1 1 var1 1 var2 1 var3` instruction (equivalent to

$1 = 1 \times occ(A1) + 1 \times occ(A2) + 1 \times occ(A3)$


## tria

>A d1 d2 angle [esd=0.02]

>B For a given set of bond pairs, sharing an atom or atom triplet generates two `DFIX` commands and one `DANG` command.

>D `CODE tria 1 1 180 C1 C2 C3` will generate `DFIX 1 0.02 C1 C2 C2 C3`.
\par
`CODE DANG 2 0.04 C1 C3` will calculate the distance for DANG from d1 d2 and the angle.


## xf.rm.ShareADP1

>A [atoms]

>B Generates the shared, rotated ADP constraint. For 3 atoms, it generates an ADP rotated around the bond e.g. around X-C bond in X-CF\low{3}. For more atoms, it creates an ADP rotated around the normal of the plane formed by the atoms.


## zoom

>B To get the current value of the scene zoom use:
`CODE echo zoom()`
To set current zoom to a certain value use:
`CODE zoom(eval(Value-zoom()))`
This can be used to put different structures on the same scale. Note that a value of 1 corresponds to the scale where the smallest dimension of the screen view is 1 Angstrom.
To reset zoom to default for the current model use:
`CODE gl.zoom`
  