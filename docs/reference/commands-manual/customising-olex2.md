---
title: "Customising Olex2"
linkTitle: "Customising Olex2"
weight: 10
description: "Things you can do to make Olex2 your own"
---
 
>A {Console, Picture_labels} 

>B Brings up the dialog to choose a font for the Console or Labels, which end up on the picture. Use the built in function 'choosefont([olex2])' to choose a system. Alternatively, a specially prepared/portable font can be used to specify the font.


## editmaterial

>A {helpcmd, helptxt, execout, error, exception, any object name available with lstgo}

>B Brings up a dialog to change properties of the specified text section or graphical object.

>C
 * **helpcmd**: the command name in the help window
 * **helptxt**: the body of the help item
 * **execout**: the output text printed in the console of external programs
 * **error**: reporting errors in the console
 * **exception**: reporting exception in the console

This command can be used to edit properties of any objects printed by `CODE lstgo` macro. An example of that could be editing material of the console text:

>D `CODE EditMaterial Console`. The the object name is case sensitive.


## save

>A {scene, style, view, model} [file_name]

>B If the file name is not provided, the *Save as...* dialog will be shown which allows you to save current settings to file. 
The scene saves current font names/sizes as well as the materials for the specific console output e.g. external programs output, error and exception reporting.
The style saves information about the appearance of objects in the scene.
The view saves current zoom and the scene orientation.
The model saves current view including the crystallographic mode and style.


## load

>A {scene, style, view, model, radii} [file_name]

>B Load one of the previously saved items. If no file name is provided, the 'Open file...' dialog will appear. If just a file name is provided (the extension will be guessed by Olex2); for styles and scene the last used folders will be used by default, the current folder will be used for the views and models. Loading radii (vdw, pers, sfil) allows the user to change the radii Olex2 uses for various calculations/display.


## grad

>A [C1 C2 C3 C4] [-p]

>B Choose the colour of the four corners of the graduated background. 

>C
 * **-p**: a file name for the picture to be placed in the background


## brad

>A *r* [hbonds]

>B Adjusts the bond radii in the display. If *hbonds* is provided as the second argument, the given radius *r* is applied to all hydrogen bonds. This operates on all or selected bonds.


## ads

>A {elp, sph, ort, std}

>B A function for drawing styles development. Changes atom drawing style for all/selected atoms.

>C
 * **elp**: represents atoms as ellipsoids (if ellipsoids are available)
 * **sph**: represents atoms as spheres
 * **ort**: same as elp, but spheres have one of the quadrants cut out
 * **std**: a stand-alone atom (i.e. shown as a cross, in wire-frame mode)


## arad

>A {sfil, pers, isot, isoth, bond, vdw}

>B A function for drawing styles development - applies different radii to all/selected atoms.

>C
 * **sfil**: sphere packing radii (as in ShelXTL XP)
 * **pers**: a fixed radii for model viewing
 * **isot**: each atom has its own radius depending on the value of the Uiso or ADP
 * **isoth**: same as isot, but the H atoms are also displayed with their real Uiso's
 * **bond**: all atoms get the same radii as default bond radius
 * **vdw**: the default/loaded Van der Waals radii used in most of the calculations


## azoom

>A % [atoms]

>B Changes the radii of all/given atoms, the change is given as a percentage.
  