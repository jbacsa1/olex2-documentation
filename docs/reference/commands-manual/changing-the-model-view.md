---
title: "Changing the Model View"
linkTitle: "Model View"
weight: 3
description: "Changing the visual appearance of the molecule"
---
 
{{% includeo "/content/en/olex2/docs/reference/commands/matr.md" %}}

{{% includeo "/content/en/olex2/docs/reference/commands/rota.md" %}}

{{% includeo "/content/en/olex2/docs/reference/commands/direction.md" %}}

{{% includeo "/content/en/olex2/docs/reference/commands/mpln.md" %}}

{{% includeo "/content/en/olex2/docs/reference/commands/zoom.md" %}}

