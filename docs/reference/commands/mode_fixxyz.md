---
title: "mode fix xyz"
weight: 9
description: Fixes the coordinates for subsequently clicked atoms
categories:
 - Commands
 - Model Building
tags:
 - refinement
---

>B Fixes the coordinates for subsequently clicked atoms.
