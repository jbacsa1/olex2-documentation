---
title: "free"
weight: 9
description: Free occupancy, xyz or Uiso
categories:
 - Commands
 - Model Building
tags:
 - refinement
 - restraints
 - xyz
 - Uiso
 - occupancy
---
>A {occu, xyz, Uiso} [atoms]

>B The opposite of FIX - makes the specified parameters for the given atoms refineable. Freeing the occupancy is also available from the context menu.
