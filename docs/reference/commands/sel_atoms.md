---
title: "sel atoms"
weight: 9
description: Olex2 selection syntax concerning atoms
categories:
 - Commands
 - Interface
tags:
 - select
---

>A where

 * **xatom.bai.mw** > 20: Select all atoms where the atomic mass is larger than 20
 * **xatom.bai.z** > 2: Select all atoms where the atomic number is greater than 2
 * `CODE atoms`: all atoms
