---
title: "direction"
weight: 9
description: Prints the current view direction
categories:
 - Commands
tags:
 - view
 - display
 - analysis
---

>B The command prints current normal in crystallographic coordinates and tries to match it to a crystallographic direction.

