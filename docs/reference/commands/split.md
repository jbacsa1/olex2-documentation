---
title: "split"
weight: 9
description: Split the selected atoms into to parts
categories:
 - Commands
 - Interface
tags:
 - disorder
 - refinement
---

>A [-r={eadp, isor, simu}]

>B Splits selected atom(s) along the longest ADP axis into two groups and links their occupancy through a free variable.

>C
 * **-r**: adds specific restraints/constraints (EADP, ISOR or SIMU) for the generated atoms
