---
title: "azoom"
weight: 9
categories:
 - Commands
description: Changes the radii of all/given atoms
tags:
 - display
---
 
>A % [atoms]

>B Changes the radii of all/given atoms, the change is given as a percentage.
