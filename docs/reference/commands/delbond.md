---
title: "delbond"
weight: 9
description: Remove a particular bond
categories:
 - Commands
 - Essential
 - Model Building
tags:
 - refinement
---

>A A1, A2 or Selected bond(s)

>B Removes selected bonds from the connectivity list. Use this command to permanently remove bonds from the display too.
