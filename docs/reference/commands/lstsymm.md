---
title: "lstsymm"
weight: 9
description: List all symmetry operations for the current structure
categories:
 - Commands
 - Analysis
tags:
 - symmetry
---


>B Prints symmetry operations and their codes for the current structure.
