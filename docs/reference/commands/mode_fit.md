---
title: "mode fit"
weight: 9
description: Fitting and splitting of fragments
categories:
 - Commands
categories:
 - Model Building
 - Essential
tags:
 - refinement
 - disorder
---

>A [-s]

>B Allows fitting of selected group (moving and rotating in 3D).

>C
 * **-s**: a new group is created at the fitted location. The occupancy of this and the original group is constrained to be 1.
