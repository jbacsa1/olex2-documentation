---
title: "hklstat"
weight: 9
description: Prints detailed information about reflections
categories:
 - Commands
 - Interface
tags:
 - refinement
 - .hkl
 - statistics
---

>B Prints detailed information about reflections used in the refinement.

