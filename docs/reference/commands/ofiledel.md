---
title: "OFileDel"
weight: 9
description: Delete the specified overlayed file
categories:
 - Commands
tags:
 - overlay
---

>B Deletes overlayed file specified by the index
