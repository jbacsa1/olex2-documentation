---
title: "kill"
weight: 9
description: Delete atoms, bonds or labels
categories:
 - Commands
 - Model Building
 - Interface
 - Essential
tags:
 - refinement
---

>A Atom names or selection or `CODE labels`

>B Deletes given or selected atoms, bonds or labels. Note that if the bonds are deleted this way they will reappear the next time the structure connectivity is updated. Use `CODE delbond` to remove bonds permanently.
