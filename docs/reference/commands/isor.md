---
title: "isor"
weight: 9
description: Restrain the ADP towards being isotropic
categories:
 - Commands
 - Model Building
tags:
 - refinement
 - disorder
 - ADP
---

>A [esd=0.1] [esd_terminal=0.2]

>B Restrains the ADP of the given atom(s) to be approximately isotropic.
