---
title: "delu"
weight: 9
description: Rigid-bond APD restraint
categories:
 - Commands
 - Model Building
tags:
 - refinement
 - restraint
 - ADP
---

>A [esd12=0.01] [esd13=0.01]

>B Rigid Bond restraint
