---
title: "Batch Processing"
linkTitle: "Batch Processing"
weight: 6
description: "Sometimes a little automation helps..."
categories:
 - Interface
tags:
 - batch processing
 - programming
---
 

There are situations in which you want Olex2 to do the same (or similar) things over and over again. Processing multiple datasets where one variable was changed systematically (pressure or temperature) is one example.


There really is no need to solve and refine the VT datasets from scratch (assuming no phase change!) -- the hkl files that are generated after each temperature run will (should) have the latest header/cell information at the end of the reflections:


{{< highlight go >}}

[...] 0   0   0    0.00    0.00

TITL Pd-OTf2-dppf in P2(1)/n
REM P2(1)/n (#14 in standard setting)
CELL 1.54184  12.260340  17.811737  17.859228  90.0000  97.7028  90.0000
ZERR    1.00   0.000229   0.000296   0.000314   0.0000   0.0017   0.0000
LATT  1
SYMM -x+1/2, y+1/2,-z+1/2
SFAC C H O F P S Fe Pd
UNIT 144.00 128.00 32.00 24.00 8.00 8.00 4.00 4.00
TREF
HKLF 4
END
{{< / highlight >}}


Olex2 will automatically use that information if you choose to refine against an hkl file that has this format. Temperature information (TEMP) does not appear to get through, so that needs to be managed manually.
I am just thinking aloud here -- yes, it might be nice to have a script that would handle all of that automatically -- but then again, maybe it's just as easy doing this manually (it's also easier to keep an eye on things!):

 - Solve/refine/finish the structure using a good dataset - say **110K_name.hkl**
 - Copy all hkl files of the same polymorph into the folder of that structure
 - Select a different hkl file, say **120K_name.hkl**
 - `file 120K_name.ins`
 - Refine
 - Repeat
 
 You will end up with a series of correctly named ins/res/cif files -- and they should all have the correct cell information. If you name things like you did name the frames, then all the meta-data associated with these runs should also come through, just make sure that you copy the relevant metacif files (**name.od_cif**, for example) together with the **name.hkl** file.