---
title: "Molecular Display"
linkTitle: "Molecular Display"
weight: 4
description: "Customise how the atoms and molecules look"
categories:
tags:
 - drawing
 - style
 - atom display
---

Olex2 provides a number of tools to customise the graphical display for picture production. Some of the customisation is described above the Colour fragments uniformly section. All graphical objects in Olex2 belong to groups. The default atom groups are groups by element type, the next, more detailed group is by atom name and the final group, which Olex2 can create automatically also include the symmetry generating the atom, thus containing a single, unique atom. It is very similar with bonds. Currently existing groups and their usage can be listed using _lstgo_ command. Opening a different file or using _default_ command clears the groups_ customisation. Typical use of the groups by atom name is for packing - customisation to atoms or bonds done in the asymmetric unit will be also included into any packing of the molecules.

Any of the Olex2 graphical objects consists of _primitives_. These can be added or removed to the object thus changing its appearance. Primitives are a part of the group into which the atoms or bond belong and therefore undergo the same convention as the material group_s properties. There are a few commands which allow operating on groups, their primitives and primitive material:

 - **Mask**: changes the primitives available in the group. The command can be applied to a selection or a named group. For example _mask C-C 0_ will make C-C bonds invisible; _mask C-C 256_ will render C-C bonds a stippled cone. The numeric value is a bitmask for the primitives to enable.
 - **GetMaterial**: returns material properties of a named primitive. For example _echo GetMaterial(N.Sphere)
 - **SetMaterial**: sets material of a named primitive to the given material string value. For example it can be used to copy material of one primitive onto the other like “SetMaterial _destination primitive name_ _material string_ where _material string_ can be a call to GetMaterial
 - **Individualise**: creates a more specialised group
 - **Collectivise**: joins a specialised group with the parent group (reverts effects of the _individualise_). Note that if no argument provided (and the selection is clear) all previous group operations are undone an only default groups remain (same as _clear style_).

There are two dialogs available to control groups, their primitives and primitive material. The dialogs are accessible through the object context menu _Graphics->Draw style_ and _Graphics->Primitives_.  The Bond and Atom primitives dialogs are shown in Figure 3. View of the bond primitives.  and Figure 4. View of the atom primitives.

{{< webp image="/images/bond_primitives.png" alt="The primitives, from which the bond display is composed." class="centered" >}}

The _Material properties_ dialog is shown below. View of the atom material properties dialog.

{{< webp image="/images/atom_primitives.png" alt="The primitives, from which the atom display is composed." class="centered" >}}

{{< webp image="/images/materials_properties.png" alt="View of the atom material properties dialog." class="centered" width="100%">}}

It is possible to group graphical objects (such as atoms or labels). Each of the grops can then be assigned different visual properties.

For example, you may want to change the label colour of some (but not all) labels in the picture. You can achieve this by selecting the labels you want to collect in a group call 'red_labels' and typing

`CODE name collection red_label`

You can then Right-Click on one of the labels in this group and select _Draw Style_. Any graphic properties you set here will apply to all labels in this group.

Alternatively, you can get the same dialoge box by typing:

`CODE editmaterial red_label`

When you `CODE fuse` the structure, or refine again, the label colour is lost. But the properties of the 'red_label' collection is remembered, so if you need to re-make an image, you just need to add the objects to the relevant collection again.

You can use this technique to make the label colour of Atom Labels different from the Bond Labels.