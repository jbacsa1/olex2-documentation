---
title: "Console Settings"
linkTitle: "Console"
weight: 4
description: "Change the appearance of the console"
categories:
 - Interface
tags:
 - console
---

Olex2 has a built in console for typing commands. Sometimes it is desirable to see only the text (output) or the structure. There are several commands and shortcuts to help with this. CTRL+T toggles whether the molecule is displayed or not. So, if your molecule has inexplicably disappeared, it's always worth pressing this key combination...

The program output of Olex2 happens 'behind' the molecule. The wisdom of doing things this way can be debated, but it means that there are fewer windows cluttering your screen. It is possible to adjust the number of lines of output you see by typing:

`CODE lines n`

 (e.g. lines 5) to see only 5 lines of the output a time, it may be confusing for procedures producing more that 5 lines (like calcvoid). If you want to see all lines, type:

`CODE lines -1`

Alternatively, you can type 'text' in the console (or use corresponding GUI links) to view the text output in an external text editor. You can always use PgUp and PgDn keys to scroll the console output. There is a limited buffer to hold the console output, however a full transcript of the Olex2 session is available in the log file, which can be displayed by using the `CODE log` command.