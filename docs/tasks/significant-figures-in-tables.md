---
title: "Significant Figures in Tables"
linkTitle: "Significant Figures"
weight: 3
description: "How many significant figures are reported with geometric parameters?"

categories:
- Interface

tags:
- report
- table
- CIF
---
 

## Symptom

Sometimes, you will find significant figures for geometric parameters with 4 significant figures:

    Co01	N00E	2.0396(17)

and sometimes there are only 3

    S002	C00J	1.845(2)

In some cases, there are even fewer!

Both of these examples are taken from the same structure -- but you may find that *all* parameters in one structure are known to a high precision while in another structure they are not.  So why is this?


## Explanation

The number of reported significant figures (decimal points, d.p) depends on the estimated standard deviation (e.s.d.) value of the parameter -- i.e. the number in round brackets behind the actual figure, 2.0396**(17)**. In crystallography, the "Rule of 19" applies: if the e.s.d value is larger than 19, one digit will be dropped and the e.s.d becomes '2'. So, instead of writing 2.0396(21), the convention is that we would report this as 2.040(2).

This is all done automatically in all refinement programs, and Olex2 just reports these figures corresponding to the precision which with the parameter in question has been determined.

For the best measurement, you want the highest number of significant figures (and therefore the lowest possible e.s.d value). This depends largely on your data quality, but can also depend on your model -- for example, how well you have modelled any disorder.

If there are only two or three significant figures in these tables, then this points to a real problem with your data and causes the corresponding CheckCif Alerts. The only way out of that is to re-measure your structure, using a different crystal. In some cases, this may also mean that your diffractometer isn't up to the job of a particular crystal and you might have to look around for alternatives.