---
title: "NoSpherA2"
linkTitle: "NoSpherA2"
weight: 8
type: docs
description: "Atoms are not spheres! Non-Spherical Atom in Olex2."
categories:
 - NoSpherA2
tags:
 - HAR
 - refinement
 - non-spherical
 - form factor
---
 
{{< webp image="/images/diagram_kreis_gui.jpg"  width="600" alt="The basic workflow of Hirshfeld Atom Refinement (HAR) in NoSPherA2">}}


It is now possible to refine routine sturucture with olex2.refine using non-spherical form factors. This technique is state-of-the art and will improve your structures significantly.

This consitutes a real step change in the way routine structures will be done in the future -- and it is here, right now, for you to try.

The mathematical, theoretical and practical details of this technique are necessarily quite complex -- but that doesn't mean that it is difficult to do for you! If you want to dig deeper we refer you to the original paper at DOI: 10.1039/D0SC05526C.

**If you use NoSpherA2 you agree to cite the following literature in your main manuscript.**

- Kleemiss et al., Chem. Sci., DOI: 10.1039/D0SC05526C
- Midgley et al., in preparation

NoSpherA2 is included with all versions of Olex2-1.3 -- in up-to date versions, it is activated by default -- ready to use! -- if your version of Olex2 isn't up to date, then please make sure you have automatic updating switched on and get the new version.

This recording of a presentation by Florian Kleemiss, the principal author of NoSpherA2, explains what this is all about. It is about an hour long, but will give you a good insight into what NoSpherA2 is and what it isn't!

{{< youtube id="Q_Xt4jvwA8Q" >}}

