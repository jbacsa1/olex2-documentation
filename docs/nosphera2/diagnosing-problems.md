---
title: "Diagnosing Problems"
linkTitle: "Troubleshooting"
date: 19-10-2020
weight: 9
description: "When things go wrong with NoSperA2"
author: Florian Kleemiss
categories:
 - NoSpherA2
tags:
 - troubleshooting


---
 
>OLEX2 We will try to keep a list of problems that one might encounter when working with NoSpherA2. Please have a look whether you can find a solution on this site before being frustrated, otherwise reach out to us, especially to [Florian](/about/#Florian), to find what went wrong.
 
## Wrong Multiplicity // Wrong damping

If your calculation is very unstable (Delta-E in QM step is not really decreasing) you might not have chosen the correct multiplicity for the wavefunctiin calculation or might have selected the wrong Damping/Conv. Strat.

In the case of Multiplicity issues it is recommended to recount electrons, especially at the metal center, if present, and think about possible high spin configurations being present. Also keep in mind that a dimer of triplet molecules will become a quintet, that is Multiplicity=5.

If you have metal centers it is also most likely recommendable to increase the value for Damping/Conv. Strat. in order to have a more subtle wavefunction convergence.
 
## Error in NoSpherA2: Unsuccesful Wavefunction Calculation!

There can be a number of reasons for this error. Somewhere on the way, the wavefunction calculation failed, and we now need to figure out *where* and *how* it failed.

### Check the ORCA output file

Please have a look at the ORCA output file in ***STRUCTUREFOLDER*/olex2/Wfn_job/*STRUCTURENAME*.out**

This file is the ORCA output file. It will contain any error messages by ORCA. Please make sure to scroll to the bottom of the file to see error messages just before the end of the program. If it is something like Error during GTOInt it most likely means your MPI installation is at failure. Did you install the parallel executables of ORCA?

### Check for multiprocessing (MPI)

You can check by going to the folder where you selected ORCA to be installed and look for **_mpi.exe** files. To check if your MPI is correctly installed open a powershell/command prompt and call **mpiexec.exe** without any other arguments. This should return something like:


![This is how the mpiexec output looks like on Windows](../nosphera2-problem_01.png)

If all of this is fine you need to make sure your wavefunction calculation input was reasonable. Check for multiplicity or charges of the system you have on screen and make sure the atoms are defined in the basis set (if Z>Kr you need **x2c-** or **jorge-** basis sets and relativistics, we will fix the choice of basis sets in case of this in upcoming updates of NoSpherA2)

If none of this helps you will need to contact us and tell us more about the example, input or output. The line "ORCA did not terminate normally" is just a generic warning to check for errors during the QM calculation.

## Nothing starts

Sometimes the variable for the multiplicity stays uninitialized in some structures when grown. Try increasing and decreasing multiplicity once to re-initiallize it.