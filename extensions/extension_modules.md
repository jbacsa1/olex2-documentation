---
title: "Extension Modules"
linkTitle: "Extension Modules"
weight: 4
status: draft
description: "Various extension modules are available for Olex2"
categories:
 - interface
tags:
 - extensions
 - drawing
 - report
---
 

- _ReportPlus_: 

![The ReportPlus Module](../reportlabelled.jpg)

This module allows the generation of highly customisable, quality structure reports, either for a single structure or for multiple structures. This is very useful when it comes to combining structural data from multiple structures to create a set of structures ready for publication.


- _DrawPlus_: This is a collection of quick drawing styles that allows the production of sophisticated structural drawings and packing diagrams. Hydrogen bonding networks and pipi bonded systems, for example, can be visualised in publication-quality drawings at the push of a button.


- _3DRendering_: This module can create the input files that are required for 3D printers in various formats.
  