---
title: "Plugins Choice Empty"
linkTitle: "Plugins Choice Empty"
weight: 10
description: "The drop-down box in Home > Extension Modules remains empty"
tags: ['troubleshooting', 'extensions']
status: draft
---

![The choices for Olex2-1.3-alpha in June 2020](/images/plugins-choice-empty.jpg)

Available extensions are displayed from the dropbox in Home > Extension Modules. If there is an empty choice like depicted in the figure here, then plese copy and paste the following line into Olex2:
```
@py "url="https://secure.olexsys.org/PluginProvider1/available?t=1.3-alpha"\nimport HttpTools\nprint(HttpTools.make_url_call(url, {}).read())"
```

If you get a 'stacktrace' ending in something like this, then we know that there is some sort of network problem.

raise urllib2.URLError(e)urllib2.URLError: <urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:726)>


``` 
import React from 'react';
```